package com.itau.geometria.formas;

public class Triangulo extends Forma {
	double ladoA;
	double ladoB;
	double ladoC;

	
public Triangulo (double paramA, double paramB, double paramC){
	ladoA = paramA;
	ladoB = paramB;
	ladoC = paramC;
}

@Override
public double calcularArea() {
	double area = 0;
	double areaTriangulo = 0;
	
    if  ((ladoA + ladoB) > ladoC  && 
         (ladoA + ladoC) > ladoB  &&
         (ladoB + ladoC) > ladoA){  
    	  area = (ladoA + ladoB + ladoC) /2;
          areaTriangulo = Math.sqrt(area * (area - ladoA) * (area - ladoB) * (area - ladoC));
     }else {
    	 throw new NullPointerException(" os lados informados não correspondem a um triangulo ");
     }
    return areaTriangulo;
}
	
}
