package com.itau.geometria.formas;

public class Retangulo extends Forma{
	double altura;
	double largura;
	double areaRetangulo;
	
	public Retangulo(double paramAltura, double paramlargura){
		altura = paramAltura;
		largura = paramlargura;
	
	}
	
	public double calcularArea() {
		areaRetangulo =  altura * largura;
        return areaRetangulo;
	}

}
