package com.itau.geometria.formas;

public class Circulo extends Forma {
	public double raio;
	
	public Circulo(double r) {
		this.raio = r;	
	}
	
	@Override
	public double calcularArea() {
		return Math.pow(raio, 2) * Math.PI;
	}
}
