package com.itau.geometria;

import java.util.List;

import com.itau.geometria.formas.Circulo;
import com.itau.geometria.formas.Forma;
import com.itau.geometria.formas.Retangulo;
import com.itau.geometria.formas.Triangulo;
import java.util.Scanner;

public class App {
	public static void main(String[] args) {
		
		Retangulo retangulo = new Retangulo(3, 3);
		double areaRetangulo = retangulo.calcularArea();
		System.out.println(areaRetangulo);
		
		Circulo circulo = new Circulo(5);
		double areaCirculo = circulo.calcularArea();
		System.out.println(areaCirculo);
		
		Scanner ler = new Scanner(System.in);
		
		
		System.out.printf("Informe valores para calcular a área do triangulo: \n");
		System.out.printf("Informe valores para calcular a área do triangulo: \n"); double areaA = ler.nextDouble();
		double areaB = ler.nextDouble();
		double areaC = ler.nextDouble();
		
		Triangulo triangulo = new Triangulo(areaA, areaB ,areaC);
		double areaTriangulo = triangulo.calcularArea();
		System.out.println(areaTriangulo);
	}
}
